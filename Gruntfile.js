
module.exports = function(grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		watch: {
			options: {
				livereload: {
					port: 35729,
				}
			},
			js: {
				files: ['src/js/*'],
				tasks: ['uglify', 'concat']
			},
			sass: {
				files: ['src/sass/*'],
				tasks: ['sass', 'cssmin']
			}
		},

		sass: {
			dist: {
				files: [{
					src: 'src/sass/screen.scss',
					dest: 'src/sass/build/screen.css',
					ext: '.css'
				}]
			}
		},

		cssmin: {
			combine: {
				files: {
					'assets/css/theme.min.css': ['src/sass/build/*.css']
				}
			}
		},

		uglify: {
			js: {
				files: {
					'assets/js/theme.min.js': ['src/js/lib/*.js', 'src/js/plugins/*.js', 'src/js/base.js']
				}
			}
		},

		concat: {
			options: {
				separator: ';',
			},
			dist: {
				src: ['src/js/config.js', 'assets/js/theme.min.js'],
				dest: 'assets/js/theme.min.js',
			},
		}


	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('default', ['sass', 'cssmin', 'uglify', 'concat', 'watch']);
	grunt.registerTask('dev', ['sass', 'cssmin', 'uglify', 'concat', 'watch']);
	grunt.registerTask('build', ['sass', 'cssmin', 'uglify', 'concat']);

};
